class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      # t.string :email
      t.string :phone
      t.string :cpf
      t.string :rg
      t.date :birthdate
      t.references :profile, null: false, foreign_key: true
      t.references :status_user, null: false, foreign_key: true
      t.references :address, null: false, foreign_key: true
      t.float :average_rating

      t.timestamps
    end
  end
end
