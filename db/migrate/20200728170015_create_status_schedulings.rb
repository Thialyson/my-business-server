class CreateStatusSchedulings < ActiveRecord::Migration[6.0]
  def change
    create_table :status_schedulings do |t|
      t.string :description

      t.timestamps
    end
  end
end
