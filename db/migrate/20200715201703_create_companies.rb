class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :fantasy_name
      t.string :email
      t.string :cnpj
      t.string :phone
      t.string :instagram
      t.string :facebook
      t.string :website
      t.references :user, null: false, foreign_key: true
      t.float :average_rating
      t.references :address, null: false, foreign_key: true
      t.references :bank_account, null: false, foreign_key: true

      t.timestamps
    end
  end
end
