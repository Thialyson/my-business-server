class CreateOperations < ActiveRecord::Migration[6.0]
  def change
    create_table :operations do |t|
      t.float :quantity
      t.float :discount
      t.float :subtotal
      t.float :total
      t.references :product, null: false, foreign_key: true
      t.references :company, null: false, foreign_key: true
      t.references :operation_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
