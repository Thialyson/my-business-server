class CreateCashes < ActiveRecord::Migration[6.0]
  def change
    create_table :cashes do |t|
      t.string :subtotal
      t.references :company, null: false, foreign_key: true

      t.timestamps
    end
  end
end
