class CreateStatusUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :status_users do |t|
      t.string :description

      t.timestamps
    end
  end
end
