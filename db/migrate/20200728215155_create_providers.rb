class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :fantasy_name
      t.string :cnpj
      t.references :address, null: false, foreign_key: true
      t.string :phone
      t.string :email
      t.string :contact
      t.references :company, null: false, foreign_key: true
      t.float :rating

      t.timestamps
    end
  end
end
