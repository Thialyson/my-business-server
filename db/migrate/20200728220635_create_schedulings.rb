class CreateSchedulings < ActiveRecord::Migration[6.0]
  def change
    create_table :schedulings do |t|
      t.datetime :date
      t.time :start_time
      t.time :finish_time
      t.references :user, null: false, foreign_key: true
      t.references :company, null: false, foreign_key: true
      t.string :observations
      t.references :status_scheduling, null: false, foreign_key: true
      t.references :service, null: false, foreign_key: true

      t.timestamps
    end
  end
end
