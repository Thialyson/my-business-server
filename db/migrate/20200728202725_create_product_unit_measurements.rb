class CreateProductUnitMeasurements < ActiveRecord::Migration[6.0]
  def change
    create_table :product_unit_measurements do |t|
      t.string :description

      t.timestamps
    end
  end
end
