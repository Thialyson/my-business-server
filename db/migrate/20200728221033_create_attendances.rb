class CreateAttendances < ActiveRecord::Migration[6.0]
  def change
    create_table :attendances do |t|
      t.datetime :date
      t.time :start_time
      t.time :finish_time
      t.float :rating
      t.float :total
      t.float :discount
      t.references :user, null: false, foreign_key: true
      t.references :company, null: false, foreign_key: true
      t.references :service, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.references :payment_method, null: false, foreign_key: true

      t.timestamps
    end
  end
end
