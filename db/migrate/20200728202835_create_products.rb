class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.float :quantity_on_stock
      t.float :price
      t.float :weight
      t.date :fabrication_date
      t.date :validity
      t.float :discount
      t.float :average_rating
      t.references :company, null: false, foreign_key: true

      t.timestamps
    end
  end
end
