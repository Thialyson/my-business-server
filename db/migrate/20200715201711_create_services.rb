class CreateServices < ActiveRecord::Migration[6.0]
  def change
    create_table :services do |t|
      t.string :name
      t.string :description
      t.float :price
      t.float :average_rating
      t.references :company, null: false, foreign_key: true

      t.timestamps
    end
  end
end
