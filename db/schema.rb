# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_29_133727) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "state"
    t.string "city"
    t.string "neighborhood"
    t.string "street"
    t.string "number"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "attendances", force: :cascade do |t|
    t.datetime "date"
    t.time "start_time"
    t.time "finish_time"
    t.float "rating"
    t.float "total"
    t.float "discount"
    t.bigint "user_id", null: false
    t.bigint "company_id", null: false
    t.bigint "service_id", null: false
    t.bigint "product_id", null: false
    t.bigint "payment_method_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_attendances_on_company_id"
    t.index ["payment_method_id"], name: "index_attendances_on_payment_method_id"
    t.index ["product_id"], name: "index_attendances_on_product_id"
    t.index ["service_id"], name: "index_attendances_on_service_id"
    t.index ["user_id"], name: "index_attendances_on_user_id"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "agency"
    t.string "account_number"
    t.float "balance_available"
    t.bigint "bank_name_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bank_name_id"], name: "index_bank_accounts_on_bank_name_id"
  end

  create_table "bank_names", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cashes", force: :cascade do |t|
    t.string "subtotal"
    t.bigint "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_cashes_on_company_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "fantasy_name"
    t.string "email"
    t.string "cnpj"
    t.string "phone"
    t.string "instagram"
    t.string "facebook"
    t.string "website"
    t.bigint "user_id", null: false
    t.float "average_rating"
    t.bigint "address_id", null: false
    t.bigint "bank_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id"], name: "index_companies_on_address_id"
    t.index ["bank_account_id"], name: "index_companies_on_bank_account_id"
    t.index ["user_id"], name: "index_companies_on_user_id"
  end

  create_table "cost_types", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "costs", force: :cascade do |t|
    t.integer "value"
    t.string "description"
    t.bigint "cost_type_id", null: false
    t.bigint "payment_method_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cost_type_id"], name: "index_costs_on_cost_type_id"
    t.index ["payment_method_id"], name: "index_costs_on_payment_method_id"
  end

  create_table "operation_types", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "operations", force: :cascade do |t|
    t.float "quantity"
    t.float "discount"
    t.float "subtotal"
    t.float "total"
    t.bigint "product_id", null: false
    t.bigint "company_id", null: false
    t.bigint "operation_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_operations_on_company_id"
    t.index ["operation_type_id"], name: "index_operations_on_operation_type_id"
    t.index ["product_id"], name: "index_operations_on_product_id"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_brands", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_colors", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_sizes", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_types", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_unit_measurements", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.float "quantity_on_stock"
    t.float "price"
    t.float "weight"
    t.date "fabrication_date"
    t.date "validity"
    t.float "discount"
    t.float "average_rating"
    t.bigint "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_products_on_company_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "providers", force: :cascade do |t|
    t.string "fantasy_name"
    t.string "cnpj"
    t.bigint "address_id", null: false
    t.string "phone"
    t.string "email"
    t.string "contact"
    t.bigint "company_id", null: false
    t.float "rating"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id"], name: "index_providers_on_address_id"
    t.index ["company_id"], name: "index_providers_on_company_id"
  end

  create_table "schedulings", force: :cascade do |t|
    t.datetime "date"
    t.time "start_time"
    t.time "finish_time"
    t.bigint "user_id", null: false
    t.bigint "company_id", null: false
    t.string "observations"
    t.bigint "status_scheduling_id", null: false
    t.bigint "service_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_schedulings_on_company_id"
    t.index ["service_id"], name: "index_schedulings_on_service_id"
    t.index ["status_scheduling_id"], name: "index_schedulings_on_status_scheduling_id"
    t.index ["user_id"], name: "index_schedulings_on_user_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.float "price"
    t.float "average_rating"
    t.bigint "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_services_on_company_id"
  end

  create_table "status_schedulings", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "status_users", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.bigint "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_stocks_on_company_id"
    t.index ["product_id"], name: "index_stocks_on_product_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "cpf"
    t.string "rg"
    t.date "birthdate"
    t.bigint "profile_id", null: false
    t.bigint "status_user_id", null: false
    t.float "average_rating"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["profile_id"], name: "index_users_on_profile_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["status_user_id"], name: "index_users_on_status_user_id"
  end

  add_foreign_key "attendances", "companies"
  add_foreign_key "attendances", "payment_methods"
  add_foreign_key "attendances", "products"
  add_foreign_key "attendances", "services"
  add_foreign_key "attendances", "users"
  add_foreign_key "bank_accounts", "bank_names"
  add_foreign_key "cashes", "companies"
  add_foreign_key "companies", "addresses"
  add_foreign_key "companies", "bank_accounts"
  add_foreign_key "companies", "users"
  add_foreign_key "costs", "cost_types"
  add_foreign_key "costs", "payment_methods"
  add_foreign_key "operations", "companies"
  add_foreign_key "operations", "operation_types"
  add_foreign_key "operations", "products"
  add_foreign_key "products", "companies"
  add_foreign_key "providers", "addresses"
  add_foreign_key "providers", "companies"
  add_foreign_key "schedulings", "companies"
  add_foreign_key "schedulings", "services"
  add_foreign_key "schedulings", "status_schedulings"
  add_foreign_key "schedulings", "users"
  add_foreign_key "services", "companies"
  add_foreign_key "stocks", "companies"
  add_foreign_key "stocks", "products"
  add_foreign_key "users", "profiles"
  add_foreign_key "users", "status_users"
end
