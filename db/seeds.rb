# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Profile.create([
#     {name: 'Administrador', description: "Administrador do Sistema" },
#     { name: 'Proprietário', description: "Dono do Negócio" },
#     { name: 'Gerente', description: "Gerente do Negócio" },
#     { name: 'Funcionário', description: "Funcionário do Negócio" },
#     { name: 'Cliente', description: "Cliente do Sistema" }
#     ])

# StatusUser.create([
#     { description: "Em Análise" },
#     { description: "Aprovado" },
#     { description: "Suspenso" },
#     { description: "Bloqueado" },
#     ])

# User.create(name: 'admin', email: "admin@gmail.com", password: '123456', profile_id: 1, status_user_id: 1)


# StatusScheduling.create([
#     { description: "Agendado" },
#     { description: "Cancelado"}
#     ])

# PaymentMethod.create([
#     { description: "Cartão de Crédito"},
#     { description: "Cartão de Débito"},
#     { description: "Dinheiro"},
#     { description: "Transferência"}
#     ])

# CostType.create([
#     { description: "Comissão Funcionário" },
#     { description: "Salário Funcionário" },
#     { description: "Fornecedores" },
#     { description: "Retirada Sócios" },
#     { description: "Imposto s/ Venda" },
#     { description: "Aluguél" },
#     { description: "Financiamento Equipe" },
#     { description: "Manutenção Veículo/Equipamento" },
#     { description: "FGTS" },
#     { description: "Energia Elétrica" },
#     { description: "Combustível" },
#     { description: "Férias" }
#     ])

# ProductType.create([
#     { description: "A" },
#     { description: "B" }
#     ])

# ProductCategory.create([
#     { description: "AA" },
#     { description: "BB" }
#     ])
# ProductBrand.create([
#     { description: "Avon" },
#     { description: "Boticário" }
#     ])
# ProductSize.create([
#     { description: "Pequeno" },
#     { description: "Médio" },
#     { description: "Grande" }
#     ])
# ProductColor.create([
#     { description: "Verde" },
#     { description: "Vermelho" },
#     { description: "Azul" }
#     ])

# ProductSize.create([
#     { description: "ml" },
#     { description: "g" },
#     { description: "l" }
#     ])

# OperationType.create([
#     { description: "Compra" },
#     { description: "Venda" }
#     ])
    