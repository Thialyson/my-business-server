require 'test_helper'

class BankNamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bank_name = bank_names(:one)
  end

  test "should get index" do
    get bank_names_url
    assert_response :success
  end

  test "should get new" do
    get new_bank_name_url
    assert_response :success
  end

  test "should create bank_name" do
    assert_difference('BankName.count') do
      post bank_names_url, params: { bank_name: { description: @bank_name.description } }
    end

    assert_redirected_to bank_name_url(BankName.last)
  end

  test "should show bank_name" do
    get bank_name_url(@bank_name)
    assert_response :success
  end

  test "should get edit" do
    get edit_bank_name_url(@bank_name)
    assert_response :success
  end

  test "should update bank_name" do
    patch bank_name_url(@bank_name), params: { bank_name: { description: @bank_name.description } }
    assert_redirected_to bank_name_url(@bank_name)
  end

  test "should destroy bank_name" do
    assert_difference('BankName.count', -1) do
      delete bank_name_url(@bank_name)
    end

    assert_redirected_to bank_names_url
  end
end
