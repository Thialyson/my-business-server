require 'test_helper'

class OperationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @operation_type = operation_types(:one)
  end

  test "should get index" do
    get operation_types_url
    assert_response :success
  end

  test "should get new" do
    get new_operation_type_url
    assert_response :success
  end

  test "should create operation_type" do
    assert_difference('OperationType.count') do
      post operation_types_url, params: { operation_type: { description: @operation_type.description } }
    end

    assert_redirected_to operation_type_url(OperationType.last)
  end

  test "should show operation_type" do
    get operation_type_url(@operation_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_operation_type_url(@operation_type)
    assert_response :success
  end

  test "should update operation_type" do
    patch operation_type_url(@operation_type), params: { operation_type: { description: @operation_type.description } }
    assert_redirected_to operation_type_url(@operation_type)
  end

  test "should destroy operation_type" do
    assert_difference('OperationType.count', -1) do
      delete operation_type_url(@operation_type)
    end

    assert_redirected_to operation_types_url
  end
end
