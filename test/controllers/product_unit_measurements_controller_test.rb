require 'test_helper'

class ProductUnitMeasurementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_unit_measurement = product_unit_measurements(:one)
  end

  test "should get index" do
    get product_unit_measurements_url
    assert_response :success
  end

  test "should get new" do
    get new_product_unit_measurement_url
    assert_response :success
  end

  test "should create product_unit_measurement" do
    assert_difference('ProductUnitMeasurement.count') do
      post product_unit_measurements_url, params: { product_unit_measurement: { description: @product_unit_measurement.description } }
    end

    assert_redirected_to product_unit_measurement_url(ProductUnitMeasurement.last)
  end

  test "should show product_unit_measurement" do
    get product_unit_measurement_url(@product_unit_measurement)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_unit_measurement_url(@product_unit_measurement)
    assert_response :success
  end

  test "should update product_unit_measurement" do
    patch product_unit_measurement_url(@product_unit_measurement), params: { product_unit_measurement: { description: @product_unit_measurement.description } }
    assert_redirected_to product_unit_measurement_url(@product_unit_measurement)
  end

  test "should destroy product_unit_measurement" do
    assert_difference('ProductUnitMeasurement.count', -1) do
      delete product_unit_measurement_url(@product_unit_measurement)
    end

    assert_redirected_to product_unit_measurements_url
  end
end
