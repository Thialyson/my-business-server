require 'test_helper'

class StatusUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @status_user = status_users(:one)
  end

  test "should get index" do
    get status_users_url
    assert_response :success
  end

  test "should get new" do
    get new_status_user_url
    assert_response :success
  end

  test "should create status_user" do
    assert_difference('StatusUser.count') do
      post status_users_url, params: { status_user: { description: @status_user.description } }
    end

    assert_redirected_to status_user_url(StatusUser.last)
  end

  test "should show status_user" do
    get status_user_url(@status_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_status_user_url(@status_user)
    assert_response :success
  end

  test "should update status_user" do
    patch status_user_url(@status_user), params: { status_user: { description: @status_user.description } }
    assert_redirected_to status_user_url(@status_user)
  end

  test "should destroy status_user" do
    assert_difference('StatusUser.count', -1) do
      delete status_user_url(@status_user)
    end

    assert_redirected_to status_users_url
  end
end
