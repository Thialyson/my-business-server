require 'test_helper'

class StatusSchedulingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @status_scheduling = status_schedulings(:one)
  end

  test "should get index" do
    get status_schedulings_url
    assert_response :success
  end

  test "should get new" do
    get new_status_scheduling_url
    assert_response :success
  end

  test "should create status_scheduling" do
    assert_difference('StatusScheduling.count') do
      post status_schedulings_url, params: { status_scheduling: { description: @status_scheduling.description } }
    end

    assert_redirected_to status_scheduling_url(StatusScheduling.last)
  end

  test "should show status_scheduling" do
    get status_scheduling_url(@status_scheduling)
    assert_response :success
  end

  test "should get edit" do
    get edit_status_scheduling_url(@status_scheduling)
    assert_response :success
  end

  test "should update status_scheduling" do
    patch status_scheduling_url(@status_scheduling), params: { status_scheduling: { description: @status_scheduling.description } }
    assert_redirected_to status_scheduling_url(@status_scheduling)
  end

  test "should destroy status_scheduling" do
    assert_difference('StatusScheduling.count', -1) do
      delete status_scheduling_url(@status_scheduling)
    end

    assert_redirected_to status_schedulings_url
  end
end
