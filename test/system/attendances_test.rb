require "application_system_test_case"

class AttendancesTest < ApplicationSystemTestCase
  setup do
    @attendance = attendances(:one)
  end

  test "visiting the index" do
    visit attendances_url
    assert_selector "h1", text: "Attendances"
  end

  test "creating a Attendance" do
    visit attendances_url
    click_on "New Attendance"

    fill_in "Company", with: @attendance.company_id
    fill_in "Date", with: @attendance.date
    fill_in "Discount", with: @attendance.discount
    fill_in "Finish time", with: @attendance.finish_time
    fill_in "Payment method", with: @attendance.payment_method_id
    fill_in "Product", with: @attendance.product_id
    fill_in "Rating", with: @attendance.rating
    fill_in "Service", with: @attendance.service_id
    fill_in "Start time", with: @attendance.start_time
    fill_in "Total", with: @attendance.total
    fill_in "User", with: @attendance.user_id
    click_on "Create Attendance"

    assert_text "Attendance was successfully created"
    click_on "Back"
  end

  test "updating a Attendance" do
    visit attendances_url
    click_on "Edit", match: :first

    fill_in "Company", with: @attendance.company_id
    fill_in "Date", with: @attendance.date
    fill_in "Discount", with: @attendance.discount
    fill_in "Finish time", with: @attendance.finish_time
    fill_in "Payment method", with: @attendance.payment_method_id
    fill_in "Product", with: @attendance.product_id
    fill_in "Rating", with: @attendance.rating
    fill_in "Service", with: @attendance.service_id
    fill_in "Start time", with: @attendance.start_time
    fill_in "Total", with: @attendance.total
    fill_in "User", with: @attendance.user_id
    click_on "Update Attendance"

    assert_text "Attendance was successfully updated"
    click_on "Back"
  end

  test "destroying a Attendance" do
    visit attendances_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Attendance was successfully destroyed"
  end
end
