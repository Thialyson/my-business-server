require "application_system_test_case"

class CostTypesTest < ApplicationSystemTestCase
  setup do
    @cost_type = cost_types(:one)
  end

  test "visiting the index" do
    visit cost_types_url
    assert_selector "h1", text: "Cost Types"
  end

  test "creating a Cost type" do
    visit cost_types_url
    click_on "New Cost Type"

    fill_in "Description", with: @cost_type.description
    click_on "Create Cost type"

    assert_text "Cost type was successfully created"
    click_on "Back"
  end

  test "updating a Cost type" do
    visit cost_types_url
    click_on "Edit", match: :first

    fill_in "Description", with: @cost_type.description
    click_on "Update Cost type"

    assert_text "Cost type was successfully updated"
    click_on "Back"
  end

  test "destroying a Cost type" do
    visit cost_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cost type was successfully destroyed"
  end
end
