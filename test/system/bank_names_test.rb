require "application_system_test_case"

class BankNamesTest < ApplicationSystemTestCase
  setup do
    @bank_name = bank_names(:one)
  end

  test "visiting the index" do
    visit bank_names_url
    assert_selector "h1", text: "Bank Names"
  end

  test "creating a Bank name" do
    visit bank_names_url
    click_on "New Bank Name"

    fill_in "Description", with: @bank_name.description
    click_on "Create Bank name"

    assert_text "Bank name was successfully created"
    click_on "Back"
  end

  test "updating a Bank name" do
    visit bank_names_url
    click_on "Edit", match: :first

    fill_in "Description", with: @bank_name.description
    click_on "Update Bank name"

    assert_text "Bank name was successfully updated"
    click_on "Back"
  end

  test "destroying a Bank name" do
    visit bank_names_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bank name was successfully destroyed"
  end
end
