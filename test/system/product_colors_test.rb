require "application_system_test_case"

class ProductColorsTest < ApplicationSystemTestCase
  setup do
    @product_color = product_colors(:one)
  end

  test "visiting the index" do
    visit product_colors_url
    assert_selector "h1", text: "Product Colors"
  end

  test "creating a Product color" do
    visit product_colors_url
    click_on "New Product Color"

    fill_in "Description", with: @product_color.description
    click_on "Create Product color"

    assert_text "Product color was successfully created"
    click_on "Back"
  end

  test "updating a Product color" do
    visit product_colors_url
    click_on "Edit", match: :first

    fill_in "Description", with: @product_color.description
    click_on "Update Product color"

    assert_text "Product color was successfully updated"
    click_on "Back"
  end

  test "destroying a Product color" do
    visit product_colors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product color was successfully destroyed"
  end
end
