require "application_system_test_case"

class OperationTypesTest < ApplicationSystemTestCase
  setup do
    @operation_type = operation_types(:one)
  end

  test "visiting the index" do
    visit operation_types_url
    assert_selector "h1", text: "Operation Types"
  end

  test "creating a Operation type" do
    visit operation_types_url
    click_on "New Operation Type"

    fill_in "Description", with: @operation_type.description
    click_on "Create Operation type"

    assert_text "Operation type was successfully created"
    click_on "Back"
  end

  test "updating a Operation type" do
    visit operation_types_url
    click_on "Edit", match: :first

    fill_in "Description", with: @operation_type.description
    click_on "Update Operation type"

    assert_text "Operation type was successfully updated"
    click_on "Back"
  end

  test "destroying a Operation type" do
    visit operation_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Operation type was successfully destroyed"
  end
end
