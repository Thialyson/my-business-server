require "application_system_test_case"

class StatusSchedulingsTest < ApplicationSystemTestCase
  setup do
    @status_scheduling = status_schedulings(:one)
  end

  test "visiting the index" do
    visit status_schedulings_url
    assert_selector "h1", text: "Status Schedulings"
  end

  test "creating a Status scheduling" do
    visit status_schedulings_url
    click_on "New Status Scheduling"

    fill_in "Description", with: @status_scheduling.description
    click_on "Create Status scheduling"

    assert_text "Status scheduling was successfully created"
    click_on "Back"
  end

  test "updating a Status scheduling" do
    visit status_schedulings_url
    click_on "Edit", match: :first

    fill_in "Description", with: @status_scheduling.description
    click_on "Update Status scheduling"

    assert_text "Status scheduling was successfully updated"
    click_on "Back"
  end

  test "destroying a Status scheduling" do
    visit status_schedulings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Status scheduling was successfully destroyed"
  end
end
