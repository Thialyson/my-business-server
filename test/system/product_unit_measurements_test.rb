require "application_system_test_case"

class ProductUnitMeasurementsTest < ApplicationSystemTestCase
  setup do
    @product_unit_measurement = product_unit_measurements(:one)
  end

  test "visiting the index" do
    visit product_unit_measurements_url
    assert_selector "h1", text: "Product Unit Measurements"
  end

  test "creating a Product unit measurement" do
    visit product_unit_measurements_url
    click_on "New Product Unit Measurement"

    fill_in "Description", with: @product_unit_measurement.description
    click_on "Create Product unit measurement"

    assert_text "Product unit measurement was successfully created"
    click_on "Back"
  end

  test "updating a Product unit measurement" do
    visit product_unit_measurements_url
    click_on "Edit", match: :first

    fill_in "Description", with: @product_unit_measurement.description
    click_on "Update Product unit measurement"

    assert_text "Product unit measurement was successfully updated"
    click_on "Back"
  end

  test "destroying a Product unit measurement" do
    visit product_unit_measurements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product unit measurement was successfully destroyed"
  end
end
