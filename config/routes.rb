Rails.application.routes.draw do
    devise_for :users, controlles: { }
    # devise_for :members do 
    #     get '/members/sign_out' => 'devise/sessions#destroy' 
    # end
    root to: 'dashboards#show'
    resources :costs
    resources :attendances
    resources :schedulings
    resources :cashes
    resources :providers
    resources :bank_accounts
    resources :bank_names
    resources :stocks
    resources :operations
    resources :operation_types
    resources :products
    resources :product_unit_measurements
    resources :product_colors
    resources :product_sizes
    resources :product_brands
    resources :product_categories
    resources :product_types
    resources :payment_methods
    resources :cost_types
    resources :status_schedulings
    resources :status_users
    resources :services
    resources :companies
    resources :users
    resources :addresses
    resources :profiles
    get '/dashboards', to: 'dashboards#show'


    post '/admins_create', to: 'users#create_user'
    get '/sessions/sign_out', to: 'sessions#destroy'
end
