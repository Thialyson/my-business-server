# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
    2.7.1

* System dependencies
    Ruby 2.7.1
    Rails 6.0.3.2
    run: gem install bundler
    run: bundler install

* Database creation
    run:
        rake db:create
        rake db:migrate
        rake db:seed

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
