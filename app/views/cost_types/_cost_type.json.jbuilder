json.extract! cost_type, :id, :description, :created_at, :updated_at
json.url cost_type_url(cost_type, format: :json)
