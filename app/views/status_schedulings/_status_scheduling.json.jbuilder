json.extract! status_scheduling, :id, :description, :created_at, :updated_at
json.url status_scheduling_url(status_scheduling, format: :json)
