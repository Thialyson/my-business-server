json.extract! operation_type, :id, :description, :created_at, :updated_at
json.url operation_type_url(operation_type, format: :json)
