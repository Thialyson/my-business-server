json.extract! scheduling, :id, :date, :start_time, :finish_time, :user_id, :company_id, :observations, :status_scheduling_id, :service_id, :created_at, :updated_at
json.url scheduling_url(scheduling, format: :json)
