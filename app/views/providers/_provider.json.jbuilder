json.extract! provider, :id, :fantasy_name, :cnpj, :address_id, :phone, :email, :contact, :company_id, :rating, :created_at, :updated_at
json.url provider_url(provider, format: :json)
