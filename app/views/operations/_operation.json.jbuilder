json.extract! operation, :id, :quantity, :discount, :subtotal, :total, :product_id, :company_id, :operation_type_id, :created_at, :updated_at
json.url operation_url(operation, format: :json)
