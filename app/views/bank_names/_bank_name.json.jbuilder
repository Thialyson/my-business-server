json.extract! bank_name, :id, :description, :created_at, :updated_at
json.url bank_name_url(bank_name, format: :json)
