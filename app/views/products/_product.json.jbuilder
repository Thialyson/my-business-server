json.extract! product, :id, :name, :description, :quantity_on_stock, :price, :weight, :fabrication_date, :validity, :discount, :average_rating, :company_id, :created_at, :updated_at
json.url product_url(product, format: :json)
