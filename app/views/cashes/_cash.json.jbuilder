json.extract! cash, :id, :subtotal, :company_id, :created_at, :updated_at
json.url cash_url(cash, format: :json)
