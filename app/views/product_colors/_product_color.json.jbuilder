json.extract! product_color, :id, :description, :created_at, :updated_at
json.url product_color_url(product_color, format: :json)
