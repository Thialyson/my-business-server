json.extract! product_unit_measurement, :id, :description, :created_at, :updated_at
json.url product_unit_measurement_url(product_unit_measurement, format: :json)
