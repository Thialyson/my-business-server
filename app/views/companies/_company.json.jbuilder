json.extract! company, :id, :name, :fantasy_name, :email, :cnpj, :phone, :instagram, :facebook, :website, :user_id, :average_rating, :address_id, :created_at, :updated_at
json.url company_url(company, format: :json)
