json.extract! attendance, :id, :date, :start_time, :finish_time, :rating, :total, :discount, :user_id, :company_id, :service_id, :product_id, :payment_method_id, :created_at, :updated_at
json.url attendance_url(attendance, format: :json)
