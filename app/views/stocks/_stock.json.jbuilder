json.extract! stock, :id, :product_id, :company_id, :created_at, :updated_at
json.url stock_url(stock, format: :json)
