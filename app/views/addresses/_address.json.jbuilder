json.extract! address, :id, :state, :city, :neighborhood, :street, :number, :latitude, :longitude, :created_at, :updated_at
json.url address_url(address, format: :json)
