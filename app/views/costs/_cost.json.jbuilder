json.extract! cost, :id, :value, :description, :cost_type_id, :payment_method_id, :created_at, :updated_at
json.url cost_url(cost, format: :json)
