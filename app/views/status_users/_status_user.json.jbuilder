json.extract! status_user, :id, :description, :created_at, :updated_at
json.url status_user_url(status_user, format: :json)
