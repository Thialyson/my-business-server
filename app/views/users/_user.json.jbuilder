json.extract! user, :id, :name, :email, :phone, :cpf, :rg, :birthdate, :profile_id, :average_rating, :created_at, :updated_at
json.url user_url(user, format: :json)
