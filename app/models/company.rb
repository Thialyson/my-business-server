class Company < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :address, optional: true
  has_many :bank_accounts
end
