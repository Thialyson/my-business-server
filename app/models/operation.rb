class Operation < ApplicationRecord
  belongs_to :product
  belongs_to :company
  belongs_to :operation_type
end
