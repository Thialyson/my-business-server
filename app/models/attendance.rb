class Attendance < ApplicationRecord
  belongs_to :user
  belongs_to :company
  belongs_to :service
  belongs_to :product
  belongs_to :payment_method
end
