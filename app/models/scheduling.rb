class Scheduling < ApplicationRecord
  belongs_to :user
  belongs_to :company
  belongs_to :status_scheduling
  belongs_to :service
end
