class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to :profile, optional: true
  belongs_to :status_user, optional: true
  belongs_to :address, optional: true

  paginates_per 10

end
