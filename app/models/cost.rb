class Cost < ApplicationRecord
  belongs_to :cost_type
  belongs_to :payment_method
end
