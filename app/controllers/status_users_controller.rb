class StatusUsersController < ApplicationController
  before_action :set_status_user, only: [:show, :edit, :update, :destroy]

  # GET /status_users
  # GET /status_users.json
  def index
    @status_users = StatusUser.all
  end

  # GET /status_users/1
  # GET /status_users/1.json
  def show
  end

  # GET /status_users/new
  def new
    @status_user = StatusUser.new
  end

  # GET /status_users/1/edit
  def edit
  end

  # POST /status_users
  # POST /status_users.json
  def create
    @status_user = StatusUser.new(status_user_params)

    respond_to do |format|
      if @status_user.save
        format.html { redirect_to @status_user, notice: 'Status user was successfully created.' }
        format.json { render :show, status: :created, location: @status_user }
      else
        format.html { render :new }
        format.json { render json: @status_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /status_users/1
  # PATCH/PUT /status_users/1.json
  def update
    respond_to do |format|
      if @status_user.update(status_user_params)
        format.html { redirect_to @status_user, notice: 'Status user was successfully updated.' }
        format.json { render :show, status: :ok, location: @status_user }
      else
        format.html { render :edit }
        format.json { render json: @status_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /status_users/1
  # DELETE /status_users/1.json
  def destroy
    @status_user.destroy
    respond_to do |format|
      format.html { redirect_to status_users_url, notice: 'Status user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status_user
      @status_user = StatusUser.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def status_user_params
      params.require(:status_user).permit(:description)
    end
end
