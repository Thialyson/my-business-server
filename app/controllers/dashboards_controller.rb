class DashboardsController < ApplicationController
    # before_action :authenticate_user!
  
    # GET /dashboards/1
    def show
        @user = current_user
        render "layouts/dashboard.html"
    end
  
end
  