class ProductUnitMeasurementsController < ApplicationController
  before_action :set_product_unit_measurement, only: [:show, :edit, :update, :destroy]

  # GET /product_unit_measurements
  # GET /product_unit_measurements.json
  def index
    @product_unit_measurements = ProductUnitMeasurement.all
  end

  # GET /product_unit_measurements/1
  # GET /product_unit_measurements/1.json
  def show
  end

  # GET /product_unit_measurements/new
  def new
    @product_unit_measurement = ProductUnitMeasurement.new
  end

  # GET /product_unit_measurements/1/edit
  def edit
  end

  # POST /product_unit_measurements
  # POST /product_unit_measurements.json
  def create
    @product_unit_measurement = ProductUnitMeasurement.new(product_unit_measurement_params)

    respond_to do |format|
      if @product_unit_measurement.save
        format.html { redirect_to @product_unit_measurement, notice: 'Product unit measurement was successfully created.' }
        format.json { render :show, status: :created, location: @product_unit_measurement }
      else
        format.html { render :new }
        format.json { render json: @product_unit_measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_unit_measurements/1
  # PATCH/PUT /product_unit_measurements/1.json
  def update
    respond_to do |format|
      if @product_unit_measurement.update(product_unit_measurement_params)
        format.html { redirect_to @product_unit_measurement, notice: 'Product unit measurement was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_unit_measurement }
      else
        format.html { render :edit }
        format.json { render json: @product_unit_measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_unit_measurements/1
  # DELETE /product_unit_measurements/1.json
  def destroy
    @product_unit_measurement.destroy
    respond_to do |format|
      format.html { redirect_to product_unit_measurements_url, notice: 'Product unit measurement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_unit_measurement
      @product_unit_measurement = ProductUnitMeasurement.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def product_unit_measurement_params
      params.require(:product_unit_measurement).permit(:description)
    end
end
