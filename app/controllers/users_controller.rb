class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /users
  # GET /users.json
  def index
    @users = User.order(:name).page params[:page]
  end

  # GET /users/1
  # GET /users/1.json
  def show
    respond_to do |format|
        format.html { render :show }
        format.json { render json: @user.as_json(
            include:[ 
                { status_user: { only: [ :id, :description ] } },
                { profile: { only: [ :id, :name ] } },
                { address: { except: [ :created_at, :updated_at ] } } 
            ],
            
            except: [ :created_at, :updated_at, :profile_id, :status_user_id, :address_id]) 
        } 
    end
  end


  # user.as_json(include: { posts: {
  #   include: { comments: {
  #                  only: :body } },
  #   only: :title } })

  # GET /users/new
  def new
    @user = User.new
    # @user.address_attributes.build
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /users_create
  # POST /users_create.json
  def create_user
    @user = User.new(user_params)
    @user.status_user_id = 1

    if params[:address][:street]
        address = Address.new(address_params)
        @user.address = address unless address.nil?
    end

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :profile_id, :phone, :cpf, :rg, :birthdate, :profile, :average_rating, :address, :status_user)
    end

    def address_params
        params.require(:address).permit(:id, :zip_code, :state, :city, :neighborhood, :street, :number, :latitude, :longitude)
    end
end
