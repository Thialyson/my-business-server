class BankNamesController < ApplicationController
  before_action :set_bank_name, only: [:show, :edit, :update, :destroy]

  # GET /bank_names
  # GET /bank_names.json
  def index
    @bank_names = BankName.all
  end

  # GET /bank_names/1
  # GET /bank_names/1.json
  def show
  end

  # GET /bank_names/new
  def new
    @bank_name = BankName.new
  end

  # GET /bank_names/1/edit
  def edit
  end

  # POST /bank_names
  # POST /bank_names.json
  def create
    @bank_name = BankName.new(bank_name_params)

    respond_to do |format|
      if @bank_name.save
        format.html { redirect_to @bank_name, notice: 'Bank name was successfully created.' }
        format.json { render :show, status: :created, location: @bank_name }
      else
        format.html { render :new }
        format.json { render json: @bank_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bank_names/1
  # PATCH/PUT /bank_names/1.json
  def update
    respond_to do |format|
      if @bank_name.update(bank_name_params)
        format.html { redirect_to @bank_name, notice: 'Bank name was successfully updated.' }
        format.json { render :show, status: :ok, location: @bank_name }
      else
        format.html { render :edit }
        format.json { render json: @bank_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bank_names/1
  # DELETE /bank_names/1.json
  def destroy
    @bank_name.destroy
    respond_to do |format|
      format.html { redirect_to bank_names_url, notice: 'Bank name was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_name
      @bank_name = BankName.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def bank_name_params
      params.require(:bank_name).permit(:description)
    end
end
