class StatusSchedulingsController < ApplicationController
  before_action :set_status_scheduling, only: [:show, :edit, :update, :destroy]

  # GET /status_schedulings
  # GET /status_schedulings.json
  def index
    @status_schedulings = StatusScheduling.all
  end

  # GET /status_schedulings/1
  # GET /status_schedulings/1.json
  def show
  end

  # GET /status_schedulings/new
  def new
    @status_scheduling = StatusScheduling.new
  end

  # GET /status_schedulings/1/edit
  def edit
  end

  # POST /status_schedulings
  # POST /status_schedulings.json
  def create
    @status_scheduling = StatusScheduling.new(status_scheduling_params)

    respond_to do |format|
      if @status_scheduling.save
        format.html { redirect_to @status_scheduling, notice: 'Status scheduling was successfully created.' }
        format.json { render :show, status: :created, location: @status_scheduling }
      else
        format.html { render :new }
        format.json { render json: @status_scheduling.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /status_schedulings/1
  # PATCH/PUT /status_schedulings/1.json
  def update
    respond_to do |format|
      if @status_scheduling.update(status_scheduling_params)
        format.html { redirect_to @status_scheduling, notice: 'Status scheduling was successfully updated.' }
        format.json { render :show, status: :ok, location: @status_scheduling }
      else
        format.html { render :edit }
        format.json { render json: @status_scheduling.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /status_schedulings/1
  # DELETE /status_schedulings/1.json
  def destroy
    @status_scheduling.destroy
    respond_to do |format|
      format.html { redirect_to status_schedulings_url, notice: 'Status scheduling was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status_scheduling
      @status_scheduling = StatusScheduling.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def status_scheduling_params
      params.require(:status_scheduling).permit(:description)
    end
end
